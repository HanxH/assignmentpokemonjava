import java.io.*;
import java.util.*;

import Pokemon.Bulbasaur;
import Pokemon.Charmander;
import Pokemon.Pokemon;
import Pokemon.Squirtle;

public class PokemonFarm {
	ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();

	private Pokemon pokemon;

	public void add(String pokemonType, String pokemonName, Pokemon pokemonInput) {

	}

	public void remove(String nameInput) {
		for (Pokemon pokemonList : pokemons) {
			if (nameInput.equals(pokemonList.getName())) {
				pokemons.remove(pokemonList);
				break;
			}
		}

		System.out.println("Deleted");
	}

	public void show() {
		for (Pokemon pokemonList : pokemons) {
			System.out.println("This " + pokemonList.getName() + " say : " + pokemonList.getSpeech() + " , weight = "
					+ pokemonList.getWeight() * pokemonList.getHeight());
		}
	}

	public void feed(int index) {
		pokemons.get(index).eatBerry();
	}

	public void rename(int index, String input) {
		pokemons.get(index).changename(input);
	}

	public String getPokemonName(int index) {
		return pokemons.get(index).getName();
	}

	public String getPokemonSpecie(int index) {
		return pokemons.get(index).getSpecies();
	}

	public String getPokemonSpeciei(int index) {
		return "FUSHIGISOU";
	}

	public String getPokemonSpeciel(int index) {
		return "LIZADON";
	}

	public String getPokemonSpeciem(int index) {
		return "KAMEIL";
	}

	public String getPokemonType(int index) {
		return pokemons.get(index).getType();
	}

	public String getPokemonAbility(int index) {
		return pokemons.get(index).getAbility();
	}

	public int getHP(int index) {
		return pokemons.get(index).getHp();
	}

	public int getCandy(int index) {
		return pokemons.get(index).getCandy();
	}

	public float getPokemonWeight(int index) {
		return pokemons.get(index).getWeight();
	}

	public float getPokemonHeight(int index) {
		return pokemons.get(index).getHeight();
	}

	public String getPokemonSound(int index) {
		return "This " + pokemons.get(index).getName() + " Say " + pokemons.get(index).getSpeech();
	}

	public int itemName() {
		return 0;
	}

}
