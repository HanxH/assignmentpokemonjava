package Item;

public abstract class Item extends absItem {
    protected int itemId;
    protected String name;
    protected String ability;

    public Item(int itemId, String itemName, String itemAbility) {
        this.itemId = itemId;
        this.name = itemName;
        this.ability = itemAbility;
    }

    @Override
    public int itemId() {
        return itemId;
    }

    @Override
    public String itemName() {
        return name;
    }

    @Override
    public String itemAbility() {
        return ability;
    }

}
