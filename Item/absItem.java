package Item;

public abstract class absItem {
    public abstract int itemId();
    public abstract String itemName();
    public abstract String itemAbility();
}
