package myBag;

import java.util.*;
import Item.*;
import Pokemon.Pokemon;

public class Bag {
	ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();

	private ArrayList<Item> items;

	public Bag() {

		items = new ArrayList<Item>();
	}

	public void add(Pokemon pokemonInput) {
		pokemons.add(pokemonInput);
		System.out.println("Added to bag");

	}

	public void remove(String nameInput) {
		for (Pokemon pokemonList : pokemons) {
			if (nameInput.equals(pokemonList.getName())) {
				pokemons.remove(pokemonList);
				break;
			}
		}

		System.out.println("Deleted");
	}

	public void addItem(int pokeball, int megaball, int hppotion, int berry) {
		for (int i = 0; i < pokeball; i++) {
			items.add(new pokeBall(0, "POKEBALL", "catchNormalPokemon"));
		}
		for (int i = 0; i < megaball; i++) {
			items.add(new pokeBall(1, "MEGABALL", "catchSpecialPokemon"));
		}
		for (int i = 0; i < hppotion; i++) {
			items.add(new regenHp(2, "HPPOTION", "regenHP"));
		}
		for (int i = 0; i < berry; i++) {
			items.add(new berry(3, "BERRY", "gainweigh"));
		}

	}

	public ArrayList<Item> getitem() {
		return items;
	}

	public void pokeBall() {
		Item A = new pokeBall(0, "POKEBALL", "catchNormalPokemon");
		items.add(A);
	}

	public void megaBall() {
		Item B = new pokeBall(1, "MEGABALL", "catchSpecialPokemon");
		items.add(B);
	}

	public void hpRegen() {
		Item C = new regenHp(2, "HPPOTION", "regenHP");
		items.add(C);
	}

	public void berry() {
		Item D = new berry(3, "BERRY", "gainweigh");
		items.add(D);
	}

	public void regenHp(Pokemon myPokemon, Bag bag) {
		for (Item i : items) {
			if (i.itemId() == 2) {
				myPokemon.useItem(2, 100, bag);
				items.remove(i);
				break;
			}
		}
	}

	public void useberry(Pokemon myPokemon, Bag bag) {
		for (Item i : items) {
			if (i.itemId() == 3) {
				myPokemon.useItem(3, 100, bag);
				items.remove(i);
				break;
			}
		}
	}

	public void showitem() {
		int pokeball = 0;
		int megaball = 0;
		int hppotion = 0;
		int berry = 0;
		for (Item number : items) {
			if (number.itemId() == 0) {
				++pokeball;
			} else if (number.itemId() == 1) {
				++megaball;
			} else if (number.itemId() == 2) {
				++hppotion;
			} else if (number.itemId() == 3) {
				++berry;
			}
		}
		System.out.println("POKEBALL = " + pokeball);
		System.out.println("MEGABALL = " + megaball);
		System.out.println("REGENHP = " + hppotion);
		System.out.println("BERRY = " + berry);
	}

	public boolean empty() {
		return items.isEmpty();
	}

}
