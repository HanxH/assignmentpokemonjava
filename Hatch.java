public class Hatch {
    private String type;

    public Hatch(String type) {
        this.type = type;
    }

    public static String getEgg() {
        return type;
    }

}