import java.util.Random;
import myBag.*;

public abstract class Pokemon {
    Random rand = new Random();
    protected String name;
    protected float weight;
    protected float height;
    protected String species;
    protected String type;
    protected String speech;
    protected int hp;
    protected int attack;
    protected int defense;
    protected int level;
    protected int candy;
    protected int exp;
    protected int maxHp;
    protected float gainWeight;
    protected float gainHeight;
    protected String ability;
    protected String gender;

    public Pokemon(String name) {
        this.name = name;
        weight = rand.nextInt(50) + 1;
        height = rand.nextInt(50) + 1;
        this.species = "species";// 3 species
        this.type = "3type";
        this.speech = "speech";// คำทักทาย tooltip
        this.hp = 1;
        this.attack = 1;
        this.defense = 1;
        this.maxHp = 1;
        this.exp = 0;
        this.candy = 1; // evolu
        this.level = 1;
        this.ability = "ability"; // ท่าไม้ตาย
        this.gainWeight = 3;
        this.gainHeight = 2;
        this.gender = "gender";

    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    public void changename(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public float getHeight() {
        return height;
    }

    public String getSpecies() {
        return species;
    }

    public String getSpeech() {
        return speech;
    }

    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public int getLevel() {
        return level;
    }

    public int getCandy() {
        return candy;
    }

    public int getExp() {
        return exp;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public String getAbility() {
        return ability;
    }

    public void status() {
        System.out.println("Name : " + name);
        System.out.println("Weigh : " + weight);
        System.out.println("Heigh : " + height);
        System.out.println("Species : " + species);
        System.out.println("type : " + type);
        System.out.println("Candy : " + candy);
        System.out.println("Weigh : " + weight);
        System.out.println("Gender : " + gender);
        System.out.println("Level : " + getLevel());
        System.out.println("Exp : " + getExp());
        System.out.println("Hp : " + getHp() + "/" + getMaxHp());
    }

    public void eatBerry() {
        weight += this.gainWeight;
        height += this.gainHeight;
        candy += 2;
    }

    public void healHp() {
        hp = maxHp;
    }

    public void setExp(int recieve) {
        exp = exp + recieve;
    }

    public void attack(Pokemon enemy) {
        System.out.println(name + " attack " + enemy.getName());
        enemy.setDamage(attack);
    }

    public void setDamage(int damage) {
        int damagecal = damage - defense;
        if (damagecal <= 0) {
            damagecal = 0;
        }
        hp = hp - damagecal;
        if (hp <= 0) {
            System.out.println("Pokemon " + name + " can't fight ");
            hp = 0;
        }
    }

    public void useItem(int itemID, int amount, Bag bag) {
        if (itemID == 2) {
            hp += amount;
            if (hp > maxHp) {
                hp = maxHp;
            }
        } else if (itemID == 3) {
            weight += this.gainWeight;
            height += this.gainHeight;
            level += 1;
            candy += 2;

        }
    }

}