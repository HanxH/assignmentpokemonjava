import java.awt.*;
import java.io.FileInputStream;
import javax.swing.*;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;
import Pokemon.*;
import Item.*;

public class Test extends JFrame

{

    private JLabel nameLabel;
    private JLabel specieLabel;
    private JLabel weightLabel;
    private JLabel heightLabel;
    private JLabel typeLabel;
    private JLabel hpLabel;
    private JLabel abilityLabel;
    private JLabel candyLabel;
    private JLabel soundLabel;
    private JComboBox pokemonListBox;
    private ArrayList<String> pokemonNameList;
    private JComboBox pokemonBaby;
    private JComboBox pokemonBabyTwo;
    private Hatch hatch;
    private int size;
    private Ivysaur ivysaur;
    private Charizard charizard;

    public Test(PokemonFarm pokemonFarm)

    {
        JFrame myFrame = new JFrame();
        myFrame.setLayout(null);
        myFrame.setTitle("POKéMON");
        myFrame.setIconImage(Toolkit.getDefaultToolkit().getImage("image/squirtle.png"));
        Font myFont = null;
        try {
            myFont = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream("image/Pokemon.ttf"));
            myFont = myFont.deriveFont(Font.BOLD, 8f);
        } catch (Exception error) {
        }
        // set color
        Color myWhite = new Color(249, 90, 87);
        Color myWhite1 = new Color(47, 172, 172);
        Color myWhite3 = new Color(245, 156, 30);
        Color myWhite4 = new Color(158, 45, 40);
        // end

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setSize(650, 550);
        tabbedPane.setLocation(10, 10);
        myFrame.add(tabbedPane);
        tabbedPane.setFont(myFont);

        // Home
        // ----- head panel ---------

        JPanel panel = new JPanel();

        panel = new JPanel();
        panel.setLayout(null);

        // FUSHIGIDANE
        JLabel imageChooserF = new JLabel(new ImageIcon(getClass().getResource("/Image/bullbasaursm.gif")));
        imageChooserF.setSize(150, 150);
        imageChooserF.setLocation(20, 50);
        panel.add(imageChooserF);
        // HITOGAME
        JLabel imageChooserH = new JLabel(new ImageIcon(getClass().getResource("/Image/charmandersm.gif")));
        imageChooserH.setSize(150, 150);
        imageChooserH.setLocation(250, 50);
        panel.add(imageChooserH);
        // ZENIGAME
        JLabel imageChooserZ = new JLabel(new ImageIcon(getClass().getResource("/Image/squirtlesm.gif")));
        imageChooserZ.setSize(150, 150);
        imageChooserZ.setLocation(450, 50);
        panel.add(imageChooserZ);
        // label
        JLabel inputYourType = new JLabel("CHOOSE YOUR BUDDY");
        inputYourType.setBounds(240, 10, 200, 23);
        panel.add(inputYourType);

        // Radio Button
        final JRadioButton radio1 = new JRadioButton("FUSHIGIDANE");
        radio1.setBounds(10, 250, 200, 23);
        panel.add(radio1);

        final JRadioButton radio2 = new JRadioButton("HITOGAME");
        radio2.setBounds(225, 250, 200, 23);
        panel.add(radio2);

        final JRadioButton radio3 = new JRadioButton("ZENIGAME");
        radio3.setBounds(435, 250, 200, 23);
        panel.add(radio3);

        // Set Group
        ButtonGroup typeInput = new ButtonGroup();
        typeInput.add(radio1);
        typeInput.add(radio2);
        typeInput.add(radio3);

        final JTextField nameinput = new JTextField();
        nameinput.setBounds(240, 300, 150, 23);
        panel.add(nameinput);
        // Button
        JButton addButton = new JButton("CHOOSE");
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                // Check Checkbox 1
                if (radio1.isSelected()) {
                    String typeInput = radio1.getText();
                    pokemonListBox.addItem(nameinput.getText());
                    pokemonFarm.add(typeInput, nameinput.getText(), null);
                    pokemonBaby.addItem(nameinput.getText());
                    pokemonBabyTwo.addItem(nameinput.getText());
                    JOptionPane.showMessageDialog(null, "You select : FUSHIGIDANE");
                } else if (radio2.isSelected()) {
                    String typeInput = radio2.getText();
                    pokemonListBox.addItem(nameinput.getText());
                    pokemonFarm.add(typeInput, nameinput.getText(), null);
                    pokemonBaby.addItem(nameinput.getText());
                    pokemonBabyTwo.addItem(nameinput.getText());
                    JOptionPane.showMessageDialog(null, "You select : HITOGAME");
                } else if (radio3.isSelected()) {
                    String typeInput = radio3.getText();
                    pokemonListBox.addItem(nameinput.getText());
                    pokemonFarm.add(typeInput, nameinput.getText(), null);
                    pokemonBaby.addItem(nameinput.getText());
                    pokemonBabyTwo.addItem(nameinput.getText());

                    JOptionPane.showMessageDialog(null, "You select : ZENIGAME");
                } else {
                    JOptionPane.showMessageDialog(null, "You not select.");
                }
                tabbedPane.setSelectedIndex(1);
            }
        });
        addButton.setBounds(240, 350, 150, 23);
        panel.add(addButton);
        panel.setBackground(myWhite4);
        tabbedPane.addTab("HOME", panel);
        tabbedPane.setIconAt(0, new ImageIcon("image/crown.png"));
        tabbedPane.setBackgroundAt(0, Color.ORANGE);
        tabbedPane.setForegroundAt(0, Color.WHITE);

        // Pokemon

        Icon imagePokemon = new ImageIcon(getClass().getResource("/Image/bullbasaursm.gif"));
        Icon imageLizardon = new ImageIcon(getClass().getResource("/Image/charmandersm.gif"));
        Icon imagePidgey = new ImageIcon(getClass().getResource("/Image/squirtlesm.gif"));
        Icon imageEgg = new ImageIcon(getClass().getResource("/Image/creature.png"));
        Icon imagePoke = new ImageIcon(getClass().getResource("/Image/starter.gif"));
        Icon imageivy = new ImageIcon(getClass().getResource("/Image/ivy.png"));
        Icon imageLiza = new ImageIcon(getClass().getResource("/Image/liz.png"));
        Icon imagemr = new ImageIcon(getClass().getResource("/Image/mei.png"));
        Icon imageHosp = new ImageIcon(getClass().getResource("/Image/joy.png"));

        JLabel imageIvy = new JLabel("");
        JLabel imageLizad = new JLabel("");
        JLabel imageMei = new JLabel("");
        JPanel newPanel = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(5, 5, 5, 5);
        JLabel imageChooser = new JLabel("");
        imageChooser.setIcon(imagePoke);

        nameLabel = new JLabel("PICK");
        specieLabel = new JLabel();
        weightLabel = new JLabel("");
        heightLabel = new JLabel("");
        typeLabel = new JLabel("");
        hpLabel = new JLabel("");
        abilityLabel = new JLabel("");
        candyLabel = new JLabel("");
        soundLabel = new JLabel("");

        pokemonNameList = new ArrayList<String>();
        pokemonListBox = new JComboBox(pokemonNameList.toArray());

        JButton feedButton = new JButton("EAT BERRY");
        JButton See = new JButton("DATA Pokemon");
        JButton chooseToMakeBabyButton = new JButton("MAKE BABY");

        JButton evlou = new JButton("EVOLUTION");
        constraints.gridx = 0;
        constraints.gridy = 0;
        newPanel.add(imageIvy, constraints);
        constraints.gridx = 0;
        constraints.gridy = 0;
        newPanel.add(imageLizad, constraints);
        constraints.gridx = 0;
        constraints.gridy = 0;
        newPanel.add(imageMei, constraints);
        constraints.gridx = 0;
        constraints.gridy = 0;
        newPanel.add(imageChooser, constraints);
        constraints.gridx = 0;
        constraints.gridy = 2;
        newPanel.add(nameLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 3;
        newPanel.add(specieLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 4;
        newPanel.add(weightLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 5;
        newPanel.add(heightLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 6;
        newPanel.add(hpLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 8;
        newPanel.add(abilityLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 7;
        newPanel.add(candyLabel, constraints);
        constraints.gridx = 0;
        constraints.gridy = 9;
        newPanel.add(soundLabel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        newPanel.add(pokemonListBox, constraints);

        constraints.gridx = 3;
        constraints.gridy = 3;
        newPanel.add(feedButton, constraints);

        constraints.gridx = 3;
        constraints.gridy = 5;
        newPanel.add(See, constraints);

        constraints.gridx = 3;
        constraints.gridy = 7;
        newPanel.add(chooseToMakeBabyButton, constraints);

        constraints.gridx = 3;
        constraints.gridy = 9;
        newPanel.add(evlou, constraints);
        chooseToMakeBabyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                // Check Checkbox 1

                tabbedPane.setSelectedIndex(2);
            }
        });

        // JPanel panelPokemon = new JPanel ( );
        // panelPokemon.setLayout (null);

        // --------finish right panel---------

        // ----Add Action /Item Listener------

        feedButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                pokemonFarm.feed(pokemonListBox.getSelectedIndex());
                weightLabel.setText("Weight : " + pokemonFarm.getPokemonWeight(pokemonListBox.getSelectedIndex()));
                heightLabel.setText("Height : " + pokemonFarm.getPokemonHeight(pokemonListBox.getSelectedIndex()));
                candyLabel.setText("Candy : " + pokemonFarm.getCandy(pokemonListBox.getSelectedIndex()));

            }
        });

        evlou.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (pokemonFarm.getCandy(pokemonListBox.getSelectedIndex()) < 12)
                    JOptionPane.showMessageDialog(null, "CAN NOT EVOLUTION YOUR CANDY NOT ENOUGH");

                else if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()).equalsIgnoreCase("HITOGAME")) {
                    charizard = new Charizard(pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()));
                    pokemonFarm.add(charizard.getCharizard(), "BABY  " + size, null);
                    pokemonListBox.addItem("LIZADON");
                    pokemonBaby.addItem("LIZADON");
                    pokemonBabyTwo.addItem("LIZADON");
                    if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()).equalsIgnoreCase("HITOGAME"))
                        imageLizad.setIcon(imageLiza);
                    // Vovo.setIcon(imageEgg);
                    JOptionPane.showMessageDialog(null, "YOU GOT LIZADON");
                }

                else if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex())
                        .equalsIgnoreCase("FUSHIGIDANE")) {
                    ivysaur = new Ivysaur(pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()));
                    pokemonFarm.add(ivysaur.getIvysaur(), "BABY " + size, null);
                    pokemonListBox.addItem("FUSHOGOSOU");
                    pokemonBaby.addItem("FUSHOGOSOU");
                    pokemonBabyTwo.addItem("FUSHOGOSOU");
                    imageIvy.setIcon(imageivy);
                    JOptionPane.showMessageDialog(null, "YOU GOT KAMEIL");
                }

                else if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()).equalsIgnoreCase("ZENIGAME")) {
                    hatch = new Hatch(pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()));
                    pokemonFarm.add(hatch.getEgg(), "BABY " + size, null);
                    pokemonListBox.addItem("KAMEIL");
                    pokemonBaby.addItem("KAMEIL");
                    pokemonBabyTwo.addItem("KAMEIL");
                    imageMei.setIcon(imagemr);

                    JOptionPane.showMessageDialog(null, "YOU GOT FUSHOGOSOU");
                }

                else
                    JOptionPane.showMessageDialog(null, "CAN NOT MAKE WITH OTHE SPECIES");

            }

        });

        See.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(pokemonListBox.getSelectedIndex());
                if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()).equalsIgnoreCase("FUSHIGIDANE"))
                    imageChooser.setIcon(imagePokemon);
                else if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()).equalsIgnoreCase("HITOGAME"))
                    imageChooser.setIcon(imageLizardon);
                else if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()).equalsIgnoreCase("ZENIGAME"))
                    imageChooser.setIcon(imagePidgey);

                nameLabel.setText("Name : " + pokemonFarm.getPokemonName(pokemonListBox.getSelectedIndex()));
                specieLabel.setText("Specie : " + pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()));
                weightLabel.setText("Weight : " + pokemonFarm.getPokemonWeight(pokemonListBox.getSelectedIndex()));
                heightLabel.setText("Height : " + pokemonFarm.getPokemonHeight(pokemonListBox.getSelectedIndex()));
                typeLabel.setText("Type : " + pokemonFarm.getPokemonType(pokemonListBox.getSelectedIndex()));
                hpLabel.setText("HP : " + pokemonFarm.getHP(pokemonListBox.getSelectedIndex()));
                abilityLabel.setText("Ability : " + pokemonFarm.getPokemonAbility(pokemonListBox.getSelectedIndex()));
                candyLabel.setText("Candy : " + pokemonFarm.getCandy(pokemonListBox.getSelectedIndex()));
                soundLabel.setText(pokemonFarm.getPokemonSound(pokemonListBox.getSelectedIndex()));

            }

        });

        newPanel.setBackground(myWhite4);
        tabbedPane.addTab("Pokemon", newPanel);
        tabbedPane.setIconAt(1, new ImageIcon("image/pokeball.png"));
        tabbedPane.setBackgroundAt(1, myWhite);
        tabbedPane.setForegroundAt(1, Color.WHITE);

        // Pokemon Center
        JPanel pokecenPanel = new JPanel(new GridBagLayout());
        GridBagConstraints constraintsPokecen = new GridBagConstraints();
        constraintsPokecen.anchor = GridBagConstraints.WEST;
        constraintsPokecen.insets = new Insets(5, 5, 5, 5);

        JLabel eggChooser = new JLabel("");
        constraintsPokecen.gridx = 0;
        constraintsPokecen.gridy = 0;
        pokecenPanel.add(eggChooser, constraintsPokecen);

        JLabel chooseMyBaby = new JLabel("CHOOSE POKEMON TO MAKE BABY");
        constraintsPokecen.gridx = 0;
        constraintsPokecen.gridy = 2;
        pokecenPanel.add(chooseMyBaby, constraintsPokecen);
        constraintsPokecen.gridx = 0;
        constraintsPokecen.gridy = 4;
        JLabel choose1 = new JLabel("CHOOSE 1");
        pokecenPanel.add(choose1, constraintsPokecen);
        constraintsPokecen.gridx = 0;
        constraintsPokecen.gridy = 6;
        JLabel choose2 = new JLabel("CHOOSE 2");
        pokecenPanel.add(choose2, constraintsPokecen);

        constraintsPokecen.gridx = 2;
        constraintsPokecen.gridy = 4;
        pokemonBaby = new JComboBox(pokemonNameList.toArray());
        pokecenPanel.add(pokemonBaby, constraintsPokecen);
        constraintsPokecen.gridx = 2;
        constraintsPokecen.gridy = 6;
        pokemonBabyTwo = new JComboBox(pokemonNameList.toArray());
        pokecenPanel.add(pokemonBabyTwo, constraintsPokecen);
        constraintsPokecen.gridx = 2;
        constraintsPokecen.gridy = 8;
        JButton MakeBabyPokemonButton = new JButton("MAKE BABY");
        pokecenPanel.add(MakeBabyPokemonButton, constraintsPokecen);
        constraintsPokecen.gridx = 2;
        constraintsPokecen.gridy = 10;
        JButton hatchYourEgg = new JButton("HATCH BABY");
        pokecenPanel.add(hatchYourEgg, constraintsPokecen);

        constraintsPokecen.gridx = 2;
        constraintsPokecen.gridy = 11;
        JLabel hatchYourEggm = new JLabel("");
        hatchYourEggm.setIcon(imageHosp);
        pokecenPanel.add(hatchYourEggm, constraintsPokecen);

        MakeBabyPokemonButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (pokemonListBox.getSelectedIndex() == pokemonBaby.getSelectedIndex())
                    JOptionPane.showMessageDialog(null, "CAN NOT THIS ONE");
                else if (pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex())
                        .equals(pokemonFarm.getPokemonSpecie(pokemonBaby.getSelectedIndex()))) {
                    hatch = new Hatch(pokemonFarm.getPokemonSpecie(pokemonListBox.getSelectedIndex()));
                    eggChooser.setIcon(imageEgg);

                    JOptionPane.showMessageDialog(null, "SUCCESSFULL");
                } else
                    JOptionPane.showMessageDialog(null, "CAN NOT USE OTHER SPECIES");

            }
        });
        hatchYourEgg.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                pokemonFarm.add(Hatch.getEgg(), "BABY  " + nameinput, charizard null);
                pokemonListBox.addItem("BABY " + nameinput);
                pokemonBaby.addItem("BABY " + nameinput);
                pokemonBabyTwo.addItem("BABY " + nameinput);
                JOptionPane.showMessageDialog(null, "SUCCESSFULL");
                eggChooser.setIcon(null);
                size++;
            }
        });
        pokecenPanel.setBackground(myWhite1);

        tabbedPane.addTab("Pokemon center ", pokecenPanel);
        tabbedPane.setIconAt(2, new ImageIcon("image/kapikon.png"));
        tabbedPane.setBackgroundAt(2, myWhite1);
        tabbedPane.setForegroundAt(2, Color.WHITE);

        // Store
        tabbedPane.addTab("Store", new JPanel());
        tabbedPane.setIconAt(3, new ImageIcon("image/pointer.png"));
        tabbedPane.setBackgroundAt(3, myWhite3);
        tabbedPane.setForegroundAt(3, Color.WHITE);

        myFrame.setSize(680, 600);
        myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        myFrame.setVisible(true);

    }

    public Boolean isNotDuplicate(String nameCheck) {

        for (String name : pokemonNameList) {
            if (name.equals(nameCheck))
                return false;
        }
        return true;

    }

}