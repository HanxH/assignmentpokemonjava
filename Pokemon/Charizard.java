package Pokemon;

public class Charizard extends Charmander {

    public Charizard(String name) {
        super(name);
        this.species = "LIZARDON";
    }

    public String getCharizard() {
        this.species = "LIZARDON";
        return "LIZARDON";

    }

}