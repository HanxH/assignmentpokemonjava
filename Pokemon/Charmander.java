package Pokemon;
public class Charmander extends Pokemon{
    public Charmander(String name)
    {
        
        super(name);
        species = "HITOGAME";
        type = "Fire";
        speech = "Burning!!";
        ability = "Blaze";
        hp = ((int)(Math.random()* 10)) + 35;
        attack = ((int)(Math.random()* 15)) + 15;
        defense = ((int)(Math.random()* 20)) + 10;
        candy = 1;
        level = 1;
        exp = 0;
        maxHp = hp;
        gender = "";
        
    }
}