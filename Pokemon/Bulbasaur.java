package Pokemon;
public class Bulbasaur extends Pokemon{
    public Bulbasaur(String name)
    {
        super(name);
        species = "FUSHIGIDANE";
        type = "Grass,Poison";
        speech = "FUNE!!";
        ability = "Overgrow";
        hp = ((int)(Math.random()* 10)) + 35;
        attack = ((int)(Math.random()* 15)) + 15;
        defense = ((int)(Math.random()* 20)) + 10;
        candy = 1;
        level = 1;
        exp = 0;
        maxHp = hp;
        gender = "";
    }
}
