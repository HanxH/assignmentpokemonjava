package Pokemon;
public class Squirtle extends Pokemon{
    public Squirtle (String name)
    {
        super(name);
        species = "ZENIGAME";
        type = "Water";
        speech = "Zeni!!";
        ability = "Torrent";
        hp = ((int)(Math.random()* 10)) + 35;
        attack = ((int)(Math.random()* 15)) + 15;
        defense = ((int)(Math.random()* 20)) + 10;
        candy = 1;
        level = 1;
        exp = 0;
        maxHp = hp;
        gender = "";
    }
}
