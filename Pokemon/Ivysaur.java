package Pokemon;
public class Ivysaur extends Pokemon{
    public Ivysaur(String name)
    {
        super(name);
        species = "IVYSAUR";
        type = "Grass,Poison";
        speech = "IM BIGGER!!";
        ability = "Overgrow";
        hp = ((int)(Math.random()* 10)) + 50;
        attack = ((int)(Math.random()* 15)) + 30;
        defense = ((int)(Math.random()* 20)) + 20;
        candy = 3;
        level = 1;
        exp = 0;
        maxHp = hp;
        gender = "";
    }

    public String getIvysaur(){
        this.species = "IVYSAUR";
        return species;
        
    }

	
}
